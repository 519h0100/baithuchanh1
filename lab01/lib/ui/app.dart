import 'package:flutter/material.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App>{

  int counter = 0;

  fectImage() {
    print('Hi there counter - $counter');
    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appwidget = new MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Image Viewer'),),
        body: Text('Display list of images'),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
            onPressed: fectImage,
        ),
      )
    );
    return appwidget;
  }

}